import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';

class AppBarWidget extends StatelessWidget implements PreferredSizeWidget {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      actions: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: Icon(
            LineIcons.shoppingCart,
            size: 25,
          ),
        ),
      ],
      title: Text('Boc Kin'),
      centerTitle: true,
    );
  }

  @override
  Size get preferredSize => new Size.fromHeight(AppBar().preferredSize.height);
}
