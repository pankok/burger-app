import 'package:burger_app/app/routes/consts_routes.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';

import 'widgets/button_sign_widget.dart';
import 'widgets/input_sign_widget.dart';

class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(
              'assets/images/splash.jpg',
            ),
            fit: BoxFit.cover,
            colorFilter: new ColorFilter.mode(
              Colors.black.withOpacity(0.2),
              BlendMode.dstATop,
            ),
          ),
        ),
        child: Stack(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  'assets/images/log.png',
                  height: 170,
                  fit: BoxFit.cover,
                ),
                Form(
                  child: Column(
                    children: [
                      InputSignWidget(
                        lab: 'NOME',
                        iconData: LineIcons.signature,
                      ),
                      InputSignWidget(
                        lab: 'TELEFONE',
                        iconData: LineIcons.phone,
                      ),
                      InputSignWidget(
                        lab: 'EMAIL',
                        iconData: LineIcons.envelope,
                      ),
                      InputSignWidget(
                        lab: 'SENHA',
                        iconData: LineIcons.unlock,
                      ),
                      ButtonSignWidget(
                        onpress: () {},
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Container(
              alignment: Alignment.bottomCenter,
              margin: EdgeInsets.only(bottom: 15),
              child: TextButton(
                onPressed: () =>
                    Navigator.of(context).pushNamed(RoutersName().signIn),
                child: Text(
                  'Já tem conta? Entre aqui!',
                  style: TextStyle(
                    inherit: false,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
