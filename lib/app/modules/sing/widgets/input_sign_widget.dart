import 'package:flutter/material.dart';

class InputSignWidget extends StatelessWidget {
  final String lab;
  final IconData iconData;
  const InputSignWidget({
    Key key,
    @required this.lab,
    @required this.iconData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 25),
      child: TextFormField(
        decoration: InputDecoration(
          prefixIcon: Icon(
            iconData,
            size: 20,
          ),
          labelText: lab,
        ),
      ),
    );
  }
}
