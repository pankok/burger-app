import 'package:flutter/material.dart';

class ButtonSignWidget extends StatelessWidget {
  final Function onpress;
  const ButtonSignWidget({
    Key key,
    @required this.onpress,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 20),
      child: ElevatedButton(
        onPressed: onpress,
        child: Padding(
          padding: const EdgeInsets.symmetric(
            vertical: 5,
            horizontal: 30,
          ),
          child: Text(
            'ENTRAR',
            style: TextStyle(fontSize: 20),
          ),
        ),
      ),
    );
  }
}
