import 'package:burger_app/app/routes/consts_routes.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';

import 'widgets/button_sign_widget.dart';
import 'widgets/input_sign_widget.dart';

class SignInPage extends StatefulWidget {
  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(
              'assets/images/splash.jpg',
            ),
            fit: BoxFit.cover,
            colorFilter: new ColorFilter.mode(
              Colors.black.withOpacity(0.2),
              BlendMode.dstATop,
            ),
          ),
        ),
        child: Stack(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  'assets/images/log.png',
                  height: 170,
                  fit: BoxFit.cover,
                ),
                Text(
                  "Bem vindo a Casa Burger",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                Text(
                  "O melhor lugar para\nvocê e sua família",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 15,
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 20,
                ),
                Form(
                  child: Column(
                    children: [
                      InputSignWidget(
                          lab: 'EMAIL', iconData: LineIcons.envelope),
                      InputSignWidget(
                        lab: 'SENHA',
                        iconData: LineIcons.unlock,
                      ),
                      ButtonSignWidget(
                        onpress: () => Navigator.of(context)
                            .pushReplacementNamed(RoutersName().home),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Container(
              alignment: Alignment.bottomCenter,
              margin: EdgeInsets.only(bottom: 15),
              child: TextButton(
                onPressed: () =>
                    Navigator.of(context).pushNamed(RoutersName().signUp),
                child: Text(
                  'Ainda não é cadastrado? Registre-se!',
                  style: TextStyle(
                    inherit: false,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
