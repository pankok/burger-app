import 'package:burger_app/app/routes/consts_routes.dart';
import 'package:flutter/material.dart';

class CardProductWidget extends StatelessWidget {
  final String imageUrl;
  final String title;
  final String subtitle;

  const CardProductWidget({
    Key key,
    @required this.imageUrl,
    @required this.title,
    @required this.subtitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.of(context)
          .pushNamed(RoutersName().detailProduct, arguments: imageUrl),
      child: Card(
        clipBehavior: Clip.antiAliasWithSaveLayer,
        margin: EdgeInsets.all(10),
        elevation: 3,
        child: Column(
          children: [
            Image.asset(
              imageUrl,
              fit: BoxFit.fitWidth,
              height: 100,
              width: double.infinity,
            ),
            Text(
              title,
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 16,
              ),
              textAlign: TextAlign.center,
            ),
            Text(
              subtitle,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                color: Colors.black87,
                fontSize: 12,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
