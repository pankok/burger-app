import 'package:flutter/material.dart';

class DetailProductPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final String nameImage = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      body: Column(
        children: [
          Container(
            height: 300,
            color: Colors.black,
            child: Image.asset(
              nameImage,
              width: MediaQuery.of(context).size.width,
              fit: BoxFit.cover,
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 25.0),
            child: Text(
              'ESSE AQUI É TOP DEMAIS',
              style: TextStyle(fontSize: 18),
            ),
          ),
        ],
      ),
    );
  }
}
