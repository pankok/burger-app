import 'package:flutter/material.dart';

class DetailPromotionPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Container(
            child: GestureDetector(
              onTap: () => Navigator.of(context).pop(),
              child: Hero(
                tag: 'hell',
                child: Image.asset('assets/images/desc.jpg'),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 25.0),
            child: Text(
              'PROMOÇÃO DA SEMANA',
              style: TextStyle(fontSize: 18),
            ),
          ),
        ],
      ),
    );
  }
}
