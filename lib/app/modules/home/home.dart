import 'package:burger_app/app/routes/consts_routes.dart';
import 'package:burger_app/app/widgets/app_bar_widget.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';

import 'widgets/card_product_widget.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(),
      body: ListView(
        children: [
          Stack(
            children: [
              GestureDetector(
                onTap: () => Navigator.of(context)
                    .pushNamed(RoutersName().detailPromotion),
                child: Hero(
                  tag: 'hell',
                  child: Image.asset('assets/images/desc.jpg'),
                ),
              ),
              Positioned(
                top: 30,
                right: 10,
                child: Text(
                  "Welcome lleh\nPromoção da semana",
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.white,
                  ),
                  textAlign: TextAlign.right,
                ),
              )
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(18.0),
            child: Card(
              color: Colors.black87,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              child: Container(
                height: 100,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Image.asset(
                      'assets/images/log.png',
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Boc King',
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w700,
                              fontSize: 20),
                        ),
                        Text(
                          'The best shake',
                          style: TextStyle(color: Colors.white),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(18.0),
            child: Text(
              'Produtos',
              style: TextStyle(
                color: Colors.black54,
                fontWeight: FontWeight.w700,
                fontSize: 25,
              ),
            ),
          ),
          GridView.count(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            crossAxisCount: 2,
            children: [
              CardProductWidget(
                imageUrl: 'assets/images/desc.jpg',
                title: 'Combo casado',
                subtitle: '01 Burger, 01 potato, 01 refri',
              ),
              CardProductWidget(
                imageUrl: 'assets/images/b.png',
                title: 'Potato Pot',
                subtitle: 'Potato com maionese caseira',
              ),
              CardProductWidget(
                imageUrl: 'assets/images/bb.png',
                title: 'Burger big',
                subtitle: 'Super burger com molho',
              ),
            ],
          ),
          Container(
            height: 30,
          )
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: 0,
        items: [
          BottomNavigationBarItem(
            icon: Icon(LineIcons.home),
            label: "Início",
          ),
          BottomNavigationBarItem(
            icon: Icon(LineIcons.heart),
            label: "Favoritos",
          ),
          BottomNavigationBarItem(
            icon: Icon(LineIcons.user),
            label: "Minha conta",
          ),
        ],
      ),
    );
  }
}
