import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'routes/consts_routes.dart';
import 'routes/routes.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        textTheme: GoogleFonts.alefTextTheme(),
        scaffoldBackgroundColor: Color(0xfff2f2f2),
        appBarTheme: AppBarTheme(
          backgroundColor: Colors.white,
          // shadowColor: Colors.blue.shade100,
          iconTheme: IconThemeData(
            color: Colors.yellow.shade700,
          ),
          elevation: 20,
        ),
        primaryColor: Colors.yellow.shade700,
        accentColor: Colors.yellow.shade700,
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(
            shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(10.0),
            ),
            primary: Colors.yellow.shade700,
          ),
        ),
        inputDecorationTheme: InputDecorationTheme(
          filled: true,
          fillColor: Colors.white.withOpacity(0.9),
          contentPadding: const EdgeInsets.symmetric(
            horizontal: 14.0,
            vertical: 5,
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: new BorderSide(color: Colors.white),
            borderRadius: new BorderRadius.circular(10),
          ),
          enabledBorder: UnderlineInputBorder(
            borderSide: new BorderSide(color: Colors.white),
            borderRadius: new BorderRadius.circular(10),
          ),
        ),
      ),
      initialRoute: RoutersName().signIn,
      routes: routes,
    );
  }
}
