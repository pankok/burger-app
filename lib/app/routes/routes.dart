import 'package:burger_app/app/modules/home/home.dart';
import 'package:burger_app/app/modules/home/pages/detail_product_page.dart';
import 'package:burger_app/app/modules/home/pages/detail_promotion_page.dart';
import 'package:flutter/material.dart';

import '../modules/sing/sign_in_page.dart';
import '../modules/sing/sign_up_page.dart';
import 'consts_routes.dart';

Map<String, Widget Function(BuildContext)> routes = <String, WidgetBuilder>{
  RoutersName().signIn: (context) => SignInPage(),
  RoutersName().signUp: (context) => SignUpPage(),
  RoutersName().home: (context) => Home(),
  RoutersName().detailProduct: (context) => DetailProductPage(),
  RoutersName().detailPromotion: (context) => DetailPromotionPage(),
};
