class RoutersName {
  final signIn = 'sign-in';
  final signUp = 'sign-up';
  final home = 'home';
  final detailProduct = 'detail-product';
  final detailPromotion = 'detail-promotion';
}
